import { DeviceSDK } from '../providers/device-sdk';
import { Module } from '@nestjs/common';

@Module({
    imports: [],
    providers: [
      DeviceSDK
    ],
    exports: [
      DeviceSDK
    ]
  })
export class DeviceSDKModule {}
