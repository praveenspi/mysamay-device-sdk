import { URLHelper } from '../helpers/url-helper';
import { Injectable } from "@nestjs/common";

import urljoin from "url-join";
import { HttpHelper, MetadataHelper, ResponseEntity } from "@neb-sports/mysamay-common-utils";

import { Device } from "@neb-sports/mysamay-device-model";

@Injectable()
export class DeviceSDK {

    urlHelper: URLHelper;

    constructor() {
    }

    init() {
        if (!this.urlHelper) {
            this.urlHelper = new URLHelper(MetadataHelper.getContextVariable());
        }
    }


    async deleteToken(token: string): Promise<ResponseEntity<any>> {
        this.init();
        let url = urljoin(this.urlHelper.baseurl, this.urlHelper.devices, token);
        let headers = this.prepareHeader();
        let resp = await HttpHelper.deleteRequest<ResponseEntity<any>>(url, {}, headers);
        return resp.body;
    }

    async getTokensByUser(userId: string): Promise<ResponseEntity<Device[]>> {
        this.init();
        let url = urljoin(this.urlHelper.baseurl, this.urlHelper.searchDevices);
        let headers = this.prepareHeader();
        let resp = await HttpHelper.postRequest<ResponseEntity<Device[]>>(url, {userId: userId}, headers);
        return resp.body;
    }

    prepareHeader() {
        let contextVar = MetadataHelper.getContextVariable();
        return {
            "X-Access-Token": contextVar.tokenHash,
            "X-Ref-Number": contextVar.refNumber
        }
    }

}
