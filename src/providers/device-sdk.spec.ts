import { HttpHelper } from '@neb-sports/mysamay-common-utils';
import { ContextVariable } from '@neb-sports/mysamay-common-utils';
import { MetadataHelper } from '@neb-sports/mysamay-common-utils';
import { Test, TestingModule } from '@nestjs/testing';

import { DeviceSDK } from "./device-sdk";



describe('DeviceSDK', () => {
    let sdk: DeviceSDK;


    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [],
            providers: [DeviceSDK],
        }).compile();

        sdk = module.get<DeviceSDK>(DeviceSDK);
    });

    it('should be defined', () => {
        expect(sdk).toBeDefined();
    });

});
