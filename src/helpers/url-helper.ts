import { ContextVariable } from "@neb-sports/mysamay-common-utils";

export class URLHelper {
    baseurl: string;
    contextPath: string = "device-srv";
    devices: string = this.contextPath + "/devices";
    searchDevices: string = this.contextPath + "/devices/search";
    

    constructor(contextVar: ContextVariable) {
        this.baseurl = contextVar.baseUrl;
    }

}